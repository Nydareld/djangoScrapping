# Présentation #

Series Scraping Site est un site développé dans le cadre d'un projet universitaire de l'iut d'olréans. Le but de se projet est de faire resortir notre capacité a utiliser le framework de developpement de site web [Django](https://www.djangoproject.com/) et le framework de crawling [Scrapy](https://scrapy.org/).

# Installation sur un environement linux#

prérequis :
- python3
- virtualenv

Créez un virtualenv :

    virtualenv -p python3 venv3django

Sourcez le fichier pour utiliser l'environement virtuel :

    soure venv3django/bin/activate

deplacez vous dans le dossier :

    cd /le/path/vers/le/projet

installez les dependeces et la base de donnée :

    pip install -r requirements.txt

    python manage.py migrate

# Features #

## crawling ##
Pour crawler le site [serieAdict](http://seriesaddict.fr/) utiliez la commande ( sous virtualenv ):

    scrapy crawl seriesAdict

## backoffice ##
Le site dipose d'un backoffice qui permet d'ajouter une série, un acteur, de lister ceux-ci. une overview dans chaque section
## Frontoffice ##
Le site dipose d'un frontoffice qui permet de voir les 3 dernières séries ajoutés, de faire une recherche en fonction dans la description et le titre d'une série, une fois la recherche fait il est possible de jeter un oeil rapide à la synopsis grace à une bulle d'aide, possibilité de consulter une série sur une page dans laquelle toutes les informations d'une série sont données comme les acteurs sous formes de miniatures tout en bas d'une page et enfin possibilité de consulter un acteur tout comme une série.

## TODO ##

Reste a faire :
- Gestion d'utilisateurs et de droits ( inconu, membre , administration )
- ajout de commentaires aux series et aux acteurs
- pages d'overview du backoffice
