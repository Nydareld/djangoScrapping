def liTextExtractor(data):
    return data.css('li::text').extract()[0]

def authorExtractor(data):
    return data.css('li> span > a::text').extract()
