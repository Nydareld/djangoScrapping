### DJANGO IMPORTS FOR SAVE SERIES ###
import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "projetdjangoIut.settings")
import django
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()



import scrapy
import json
from scrapy import FormRequest
from .extractors import *
from series.models import Serie, Actor
# from projetdjangoIut.series.models import Serie

class SeriesAdictSpider(scrapy.Spider):

    name = "seriesAdict"
    baseUrl = "http://seriesaddict.fr"
    actors = []
    knownProperties = {
        "Créé par :" : {
            'fieldName' : "creators",
            'extractor' : "authorExtractor"
        },
        "Année de création :" : {
            'fieldName' : "year",
            'extractor' : "liTextExtractor"
        },
        "Statut :" : {
            'fieldName' : "status",
            'extractor' : "liTextExtractor"
        },
        "Titre VF :" : {
            'fieldName' : "french_title",
            'extractor' : "liTextExtractor"
        },
        "Production :" : {
            'fieldName' : "production",
            'extractor' : "liTextExtractor"
        },
        "Format :" : {
            'fieldName' : "format",
            'extractor' : "liTextExtractor"
        },
        "Chaîne(s) :" : {
            'fieldName' : "chanel",
            'extractor' : "liTextExtractor"
        },
        "Genre :" : {
            'fieldName' : "gender",
            'extractor' : "liTextExtractor"
        },
        "Nombre de saison(s) :" : {
            'fieldName' : "season_count",
            'extractor' : "liTextExtractor"
        },
        "Nombre d'épisode(s) :" : {
            'fieldName' : "episod_count",
            'extractor' : "liTextExtractor"
        },
        "Date de première diffusion :" : {
            'fieldName' : "start_date",
            'extractor' : "liTextExtractor"
        }
    }

    def start_requests(self):
        urls = [
            'http://seriesaddict.fr/serie'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        #On cherche les valeurs d'annes possibles
        years = response.css('#SerieSearchForm_annee > option').xpath('@value').extract()[1:]
        for year in years:
            #Pour chaque annee on crawl la premiere page de la recherce
            yield FormRequest.from_response(response, formname='news-form', formdata={'SerieSearchForm[annee]':year}, callback=self.parseSearchResponse)

    def parseSearchResponse(self, response):

        #On crawl toutes les séries de la page
        for encadre in response.css('#list > ul > li'):
            yield scrapy.Request(url=self.baseUrl+encadre.xpath('a/@href').extract()[0], callback=self.parseSerie)

        #Si il y a une page suivante, on fais la meme
        nextUrl = response.css('#yw0 > li.next > a').xpath('@href').extract()[0]
        if nextUrl:
            yield scrapy.Request(url=self.baseUrl+nextUrl, callback=self.parseSearchResponse)


    def parseSerie(self, response):

        #On récupere les infos globales
        title = response.css("#page > div:nth-child(3) > div.col-lg-8.col-md-8 > h1 > strong::text").extract()[0]
        description = response.css("#page > div:nth-child(3) > div.col-lg-8.col-md-8 > div.row.bg-grey.fiche-synopsis > p::text").extract()[0]
        img = self.baseUrl+response.css("#page > div:nth-child(3) > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-3.col-md-3.col-sm-3.col-xs-3 > img")[0].xpath("@src").extract()[0]

        #On récupere le tableau de propriétés avec les bonnes fonctions d'extractions
        properties = self.parseProperties(response.css("#page > div:nth-child(3) > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-6.col-md-6.col-sm-6.col-xs-9 > ul"))
        tags = response.css("#page > div:nth-child(3) > div.col-lg-8.col-md-8 > div:nth-child(2) > div:nth-child(3) > a::text").extract()

        # On cherche les acteurs
        actorsUrl = response.css('#casting > ul:nth-child(2n+1) > li > a').xpath('@href').extract()
        actors = []

        # Pour chaque acteur on l'ajoute aux acteurs de la série
        for url in actorsUrl:
            actorId = str.split(url,"/")[-1]
            actors.append(actorId)

            # Si l'acteur a pas été crawlé, on le crawl
            if actorId not in self.actors :
                self.actors.append(actorId)
                yield scrapy.Request(url=self.baseUrl+url, callback=self.parseActor)


        #On enregistre les objets dans la bd Django
        serie = Serie()

        serie.name = title
        serie.description = description
        serie.image = img
        serie.url = response.url
        serie.json_properties = json.dumps(properties)
        serie.json_tags = json.dumps(tags)
        serie.json_actors = json.dumps(actors)

        serie.save()

        yield {
            "type" : "serie",
            "title" : title,
            "original_url" : response.url,
            "image_url" : img,
            "properies" : properties,
            "description" : description,
            "tags" : tags,
            "actors" : actors
        }

    def parseActor(self, response):

        # Datas de l'acteur
        name = response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(1) > span::text").extract()[0]
        nationality = response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(2)::text").extract()[0]
        bornCity = response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(3)::text").extract()[1]
        birthdate = response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(3) > span::text").extract()[0]
        size='undef'

        if len(response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(4)::text").extract())>0 :
            size = response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-9.col-md-9.col-sm-9.col-xs-9 > ul > li:nth-child(4)::text").extract()[0]
        img = self.baseUrl+response.css("#page > div > div.col-lg-8.col-md-8 > div:nth-child(2) > div.col-lg-3.col-md-3.col-sm-3.col-xs-3 > img")[0].xpath("@src").extract()[0]

        #création de l'acteur Django
        actor = Actor()

        actor.name = name
        actor.nationality = nationality
        actor.bornCity = bornCity
        actor.birthdate = birthdate
        actor.size = size
        actor.image = img
        actor.url = response.url
        actor.originalId = str.split(response.url,"/")[-1]

        actor.save()

        yield {
            "type" : "actor",
            "name" : name,
            "nationality" : nationality,
            "bornCity" : bornCity,
            "birthdate" : birthdate,
            "size" : size,
            "img" : img,
            "original_url" : response.url,
            "original_id" : str.split(response.url,"/")[-1]
        }


    def parseProperties(self, properies):
        res = dict()

        #Pour chaque propriété de la liste, si elle est connue on l'analyse correctement sinon on la récupere en "brute"
        for property in properies.css('li'):
            field = self.knownProperties[property.css('strong::text').extract()[0]]
            if field:
                res[field['fieldName']] = globals()[field['extractor']](property)
            else:
                res[property.css('strong::text').extract()[0]] = globals()[liTextExtractor](property)
        return res
