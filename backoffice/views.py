from django.shortcuts import render
from .view import *
from series.models import Serie, Actor


# Create your views here.

def home(request):
    return render(request,"backoffice/home.html",{})

def scraping(request, scraperName):
    return render(request,"backoffice/home.html",{})

def dropAll(request):
    Serie.objects.all().delete()
    Actor.objects.all().delete()
    return HttpResponseRedirect(reverse('bo-home'))
