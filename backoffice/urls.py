"""GestionTaches URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
1. Add an import:  from my_app import views
2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
1. Add an import:  from other_app.views import Home
2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
1. Import the include() function: from django.conf.urls import url, include
2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from . import views

urlpatterns = [

    # Home backoffice
    url(r'^$',views.home,name="bo-home"),

    # Routes series
    url(r'^series/?$',views.series,name="bo-series"),
    url(r'^series/list/?$',views.listSeries,name="bo-list-series"),
    url(r'^series/newSerie/?$',views.newSerie,name="bo-new-serie"),
    url(r'^series/editSerie/(?P<name>.+)$',views.editSerie,name="bo-edit-serie"),
    url(r'^series/deleteSerie/(?P<name>.+)$',views.deleteSerie,name="bo-delete-serie"),
    url(r'^series/viewSerie/(?P<name>.+)$',views.viewSerie,name="bo-view-serie"),

    # Routes acteurs
    url(r'^actors/?$',views.actors,name="bo-actors"),
    url(r'^actors/list/?$',views.listActors,name="bo-list-actors"),
    url(r'^actors/newActor/?$',views.newActor,name="bo-new-actor"),
    url(r'^actors/editActor/(?P<name>.+)$',views.editActor,name="bo-edit-actor"),
    url(r'^actors/deleteActor/(?P<name>.+)$',views.deleteActor,name="bo-delete-actor"),
    url(r'^actors/viewActor/(?P<name>.+)$',views.viewActor,name="bo-view-actor"),

    # Routes scraping
    url(r'^scraping/(?P<scraperName>.+)/?$',views.scraping,name="bo-scraping"),

    # Dev Routes
    url(r'^dropAll/?$',views.dropAll,name="bo-drop-all")
]
