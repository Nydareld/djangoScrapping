from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from series.models import Serie,Actor
from series.forms import SerieForm

# backoffice series views

def series(request):
    series=Serie.objects.order_by('-id')[:3]
    nbSeries=len(Serie.objects.all())
    nbActeurs=len(Actor.objects.all())
    return render(request,"backoffice/series/home.html",{
        'lastSix':series,
        'nbSeries':nbSeries,
        'nbActeurs':nbActeurs
    })

def listSeries(request):
    series=Serie.objects.all()
    return render(request,"backoffice/series/list.html",{'series':series})

def newSerie(request):
    if request.method == 'POST':

        form = SerieForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bo-list-series'))
    else:
        form = SerieForm()
        return render(request,"backoffice/series/new.html",{"form":form})



def editSerie(request,name):
    serie = Serie.objects.get(name = name)
    if request.method == 'POST':

        form = SerieForm(request.POST,instance=serie)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bo-list-series'))
    else:
        form = SerieForm(instance=serie)
        return render(request,"backoffice/series/new.html",{"form":form,"edit":True})

def deleteSerie(request,name):
    serie = Serie.objects.get(name = name)
    serie.delete()
    return HttpResponseRedirect(reverse('bo-list-series'))

def viewSerie(request,name):
    serie = Serie.objects.get(name = name)
    return render(request,"backoffice/series/view.html",{"serie": serie})
