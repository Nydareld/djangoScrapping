from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from series.models import Serie,Actor
from series.forms import ActorForm

# backoffice actors views

def actors(request):
    actors=Actor.objects.order_by('-id')[:3]
    nbSeries=len(Serie.objects.all())
    nbActeurs=len(Actor.objects.all())
    return render(request,"backoffice/actors/home.html",{
        'lastSix':actors,
        'nbSeries':nbSeries,
        'nbActeurs':nbActeurs
    })

def listActors(request):
    actors=Actor.objects.all()
    return render(request,"backoffice/actors/list.html",{'actors':actors})

def newActor(request):
    if request.method == 'POST':

        form = ActorForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bo-list-actors'))
    else:
        form = ActorForm()
        return render(request,"backoffice/actors/new.html",{"form":form})



def editActor(request,name):
    serie = Actor.objects.get(name = name)
    if request.method == 'POST':

        form = ActorForm(request.POST,instance=serie)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bo-list-actors'))
    else:
        form = ActorForm(instance=serie)
        return render(request,"backoffice/actors/new.html",{"form":form,"edit":True})

def deleteActor(request,name):
    serie = Actor.objects.get(name = name)
    serie.delete()
    return HttpResponseRedirect(reverse('bo-list-actors'))

def viewActor(request,name):
    actor = Actor.objects.get(name = name)
    return render(request,"backoffice/actors/view.html",{"actor": actor})
