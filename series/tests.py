from django.core.urlresolvers import resolve
from django.test import TestCase
from .models import Serie
from .views import *

# Create your tests here.

class ModelsTests(TestCase):

    ''' Test unitaire bidon'''
    def test_concatene(self):
        self.assertEqual("Bon"+"jour", "Bonjour")

    ''' Test de création de série '''
    def test_creation_serie(self):
        serie = Serie()

    ''' Test de création et supression de série '''
    def test_creation_serie(self):
        serieDeTest = Serie()

        serieDeTest.name = "Serie de Test"
        serieDeTest.description = "test déscription"

        serieDeTest.save()

        searchedSerie = Serie.objects.get(name="Serie de Test")
        self.assertEqual(searchedSerie.name,"Serie de Test")

        searchedSerie.delete()

        searchedSerie = Serie.objects.filter(name="Serie de Test")
        self.assertEqual(len(searchedSerie),0)

class UrlsTests(TestCase):

    base = "/series/"
    routes = {
        "":home
    }

    '''Test unitaire de la page accueil sur la racine du projet'''
    def test_all_root_url_resolves_to_view(self):
        for route,function in self.routes.items():
            found = resolve(self.base+route)
            self.assertEqual(found.func,function)
