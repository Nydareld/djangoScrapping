from django.db import models
import json

# Create your models here.

class Serie(models.Model):

    name = models.CharField(max_length=250,unique=True,default="")
    description = models.TextField(blank=True)
    image = models.TextField(blank=True)
    url = models.TextField(blank=True)
    json_tags = models.TextField(blank=True)
    json_properties = models.TextField(blank=True)
    json_actors = models.TextField(blank=True)
    french_properies = {
        "creators" : "Créé par :" ,
        "year" : "Année de création :" ,
        "status" : "Statut :" ,
        "french_title" : "Titre VF :" ,
        "production" : "Production :" ,
        "format" : "Format :" ,
        "chanel" : "Chaîne(s) :" ,
        "gender" : "Genre :" ,
        "season_count" : "Nombre de saison(s) :" ,
        "episod_count" : "Nombre d'épisode(s) :" ,
        "start_date" : "Date de première diffusion :"
    }

    def tags(self):
        return json.loads(self.json_tags)

    def properties(self):
        return json.loads(self.json_properties)

    def frenchPropertyArray(self):
        res = []
        for property in self.properties():
            res.append(self.frenchPropertyValue(property))
        return res

    def frenchPropertyValue(self,property):
        value = self.properties()[property]
        if isinstance(value, list) :
            value = ", ".join(value)
        return self.french_properies[property]+value


    def searchData(self):
        return [ self.frenchPropertyValue("french_title")]

    def getActors(self):
        actorsId = json.loads(self.json_actors)
        actors = [ Actor.objects.get(originalId = id ) for id in actorsId ]
        return actors

class Actor(models.Model):

    name = models.CharField(max_length=250,unique=True,default="")
    nationality = models.CharField(blank=True,max_length=250,default="")
    bornCity = models.CharField(blank=True,max_length=250,default="")
    birthdate = models.CharField(blank=True,max_length=250,default="")
    size = models.CharField(blank=True,max_length=250)
    image = models.TextField(blank=True)
    url = models.TextField(blank=True)
    originalId = models.IntegerField(null=True)
