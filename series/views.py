from django.shortcuts import render
from .models import Serie,Actor
from watson import search as watson
from .forms import SearchForm


# Create your views here.

def home(request):
    series=Serie.objects.all()[0:3]
    return render(request,"series/home.html",{"series":series})

def contact(request):
    return render(request,"series/contact.html")

def recherche(request):
    search_results = []
    form = SearchForm()

    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            search_results = watson.filter(Serie,form.cleaned_data["searchWords"])

    return render(request,"series/recherche.html",{
        "data":search_results,
        "form":form
    })


def viewSerie(request,name):
    serie = Serie.objects.get(name = name)
    return render(request,"series/view.html",{"serie": serie})

def viewActor(request,name):
    actor = Actor.objects.get(name = name)
    return render(request,"series/viewact.html",{"actor": actor})
