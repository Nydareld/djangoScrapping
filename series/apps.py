from django.apps import AppConfig
from watson import search as watson

class SeriesConfig(AppConfig):
    name = 'series'
    def ready(self):
        Serie = self.get_model("Serie")
        watson.register(Serie.objects.all(), fields=("name", "description","properties","tags"))
