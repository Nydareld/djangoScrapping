from django.forms import ModelForm
from .models import Serie, Actor
from django import forms


class SerieForm(ModelForm):
    class Meta:
        model = Serie
        fields = ("name",
         "description")
        labels = {
            "name": "Nom de la série",
            "description": "Description de la série",
        }

class ActorForm(ModelForm):
    class Meta:
        model = Actor
        fields = ("name", "nationality", "bornCity","birthdate","size","image")
        labels = {
            "name": "Nom",
            "nationality": "Nationalité",
            "bornCity": "Ville de naissance",
            "birthdate": "Date d'aniversaire",
            "size": "Taille",
            "image": "Url de la photo"
        }


class SearchForm(forms.Form):
    searchWords = forms.CharField(label='Recherche :', max_length=300)
